package com.crudApi.CrudApi;

import com.sun.org.apache.xml.internal.utils.ThreadControllerWrapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudApiApplication {

	public static void main(String[] args) throws InterruptedException{

		SpringApplication.run(CrudApiApplication.class, args);
	}

}
