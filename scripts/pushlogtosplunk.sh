#!/bin/bash

#CI_PROJECT_ID=14210996
#CI_PIPELINE_ID=450326966
#SPLUNK_TOKEN='fe86ff4a-b069-4450-ad32-1c59b2cabb7c'
#PRIVATE_TOKEN='glpat-q7jsG2-gHkaLykGsyzmx'

#whole_data=$(curl -X GET \
#  https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs \
#  -H "private-token: $PRIVATE_TOKEN")
#
#
#response=$(curl "http://localhost:8088/services/collector" \
#    -H "Authorization: Splunk $SPLUNK_TOKEN" \
#    -d "{ \"sourcetype\": \"GITLAB_JOB_STATUS\", \"event\": $whole_data }")
#
#echo $response
#
job_ids=$(curl -X GET \
  https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs \
  -H 'private-token: glpat-q7jsG2-gHkaLykGsyzmx' | jq -r '.[] | select( .status != "running") | .id' )

#echo $job_ids | jq .

mkdir logs
for job_id in $job_ids; do
  logs=$(curl -X GET \
    https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/$job_id/trace \
    -H "private-token: $PRIVATE_TOKEN")

  echo $logs >>./logs/$job_id.log
done
